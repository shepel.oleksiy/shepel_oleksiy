import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css']
})
export class ShopComponent implements OnInit {
  brand = {};
  constructor(private route:ActivatedRoute) { }

  ngOnInit() {
    this.brand = this.route.params.subscribe(p => {
      this.brand = p.brand;
    });
  }

}
