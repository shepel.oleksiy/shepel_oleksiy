import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-brand-list',
  templateUrl: './brand-list.component.html',
  styleUrls: ['./brand-list.component.css']
})
export class BrandListComponent implements OnInit {
  config = {};

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.config = this.route.snapshot.data['config'];
  }

}
