import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ShopComponent } from './shop/shop.component';
import { HttpClientModule } from '@angular/common/http';
import { BrandListComponent } from './brand-list/brand-list.component';
import { BrandListResolverService } from './brand-list-resolver.service';

@NgModule({
  declarations: [
    AppComponent,
    ShopComponent,
    BrandListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [BrandListResolverService],
  bootstrap: [AppComponent]
})
export class AppModule { }
