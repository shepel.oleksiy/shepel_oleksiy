import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { ShopComponent } from './shop/shop.component';
import { BrandListComponent } from './brand-list/brand-list.component';
import { BrandListResolverService } from './brand-list-resolver.service';

const routes: Routes = [
  {path: '', component: BrandListComponent, resolve: {config: BrandListResolverService}},
  {path: 'shop/:brand', component: ShopComponent},
  {path: 'shop', component: ShopComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
