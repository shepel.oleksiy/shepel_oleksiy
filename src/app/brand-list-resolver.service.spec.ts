import { TestBed } from '@angular/core/testing';

import { BrandListResolverService } from './brand-list-resolver.service';

describe('BrandListResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BrandListResolverService = TestBed.get(BrandListResolverService);
    expect(service).toBeTruthy();
  });
});
