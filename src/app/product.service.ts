import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  getConfig(): Observable<any>
  {
    return this.http.get("https://us-central1-epam-hiring-event.cloudfunctions.net/options");
  }

  getAllProducts(): Observable<any>
  {
    return this.http.get("https://us-central1-epam-hiring-event.cloudfunctions.net/list");
  }
}
